#' @title exemple squirrels data
#' @description exemple dataset of squirrels in America
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{\code{age}}{character age of squirrel}
#'   \item{\code{primary_fur_color}}{character a furr color}
#'   \item{\code{activity}}{character an activity he is doing}
#'   \item{\code{counts}}{double how much of doing activity}
#'}
#' @details DETAILS
"data_act_squirrels"
